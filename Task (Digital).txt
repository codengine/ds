Python:

Write the complete python program that allows a user choose whether they want 
to calculate voltage, current or resistance. Call the class Digital.

Create necessary setter and getter methods and private member variables called 
res, volt and curr.

Allow user to repeatedly run the program until they decide to exit. 

_______________________________________________________________________________________

I have written this in C++ (example):

#include <iostream>

using namespace std;

class Digital
{
public:
  Digital();                         // default constructor
  Digital(float, float, float);     // constructor

  void set_res(float);                 // mutuator functions
  void set_current(float); 
  void set_voltage(float); 

  float get_res();                   // accessor functions
  float get_current();
  float get_voltage();

  void get_values(int);               

  float calc_res();                  // return the result in the calculation function
  float calc_current(); 
  float calc_voltage();
  void output();
private:
  float res, curr, volt;
}; 



int main() 

{
    int choice;
    char response;
    do
    {
    
    cout <<"______________________________" << endl;
    cout << "  Digital-Voltmeter Program\n";
    cout <<"______________________________" << endl;
      
    int menu();
    double value1, value2;
    cout << "Enter two values:\n";
    cin >> value1 >> value2;
    
    int m=menu();
    if (m==1)
    {
        cout << value1 << " Voltage and " << value2 << " Current is " << value1 / value2 << endl;
    }
    if (m==2)
    {
          cout << value1 << " Voltage and " << value2 << " Resistance is " << value1 / value2 << endl;
    }
    if (m==3)
    {
        cout << value1 << " Current and " << value2 << " Resistance is " << value1 * value2 << endl;
    }
    if(m!=1 || m!=2 || m!=3)
         
          cout <<"\nDo you wish to go again (Y/N):\n";
          cin >> response;

    
	}while(response == 'Y' || response == 'y');
return 0;
}

int menu ()                                
{
    int choice;                            
    do
    {
    cout << "\n Enter the Option you want to Calculate \n";
    cout << "1. For Resistance \n";
    cout << "2. For Current \n";
    cout << "3. For Voltage \n";
    cin >> choice;
    }
    while (choice < 1 || choice > 3 );
    
return choice;
}

Digital::Digital()
{
    // do nothing
}

Digital::Digital(float r,float c,float v)
{
    res = r;
      curr = c;
     volt = v;
}

void Digital::set_res(float r)            // mutuator functions
{
    res = r;
}

void Digital::set_current(float c)
{
    curr = c;
}

void Digital::set_voltage(float v)
{
    volt = v;
}

float Digital::get_res()                // accessor functions
{
    return res;
}

float Digital::get_current()
{
    return curr;
}

float Digital::get_voltage()
{
    return volt;
}

float Digital::calc_res()
{
    cout << "res is: " <<  get_voltage()  / get_current()   << "\n";
}
void Digital::output()
{
    
}

float Digital::calc_current()
{
    cout << "curre is: " <<  get_voltage()  / get_res()   << "\n";

    cout << "Current is " << endl;
}

float Digital::calc_voltage()
{
    volt = curr * res;

    cout << "Voltage is " << endl;
};
