class Digital(object):

    def __init__(self, *args, **kwargs):
        self.__voltage = None
        self.__current = None
        self.__resistance = None

    def set_voltage(self, voltage):
        self.__voltage = voltage

    def set_current(self, current):
        self.__current = current

    def set_resistance(self, resistance):
        self.__resistance = resistance

    def get_voltage(self):
        return self.__voltage

    def get_current(self):
        return self.__current

    def get_resistance(self):
        return self.__resistance
    
    def calculate_resistance(self):
        # V = IR, R = V/I
        if not self.__current:
            self.__resistance = None
        elif self.__voltage is None:
            self.__resistance = None
        else:
            self.__resistance = self.__voltage / self.__current

    def calculate_current(self):
        if not self.__resistance:
            self.__current = None
        elif self.__voltage is None:
            self.__current = None
        else:
            self.__current =self.__voltage / self.__resistance

    def calculate_voltage(self):
        if not self.__resistance:
            self.__voltage = None
        elif self.__current is None:
            self.__voltage = None
        else:
            self.__voltage = self.__current * self.__resistance


class Program(object):
    def __init__(self):
        pass

    def menu(self, prompt, choices):
        print("-------------Digital-Voltmeter Program-------------\n")
        print("\n%s\n" % prompt)
        count = len(choices)
        for i in range(count):
            print("(%s) %s" % (i + 1, choices[i]))
        print("---------------------------------------------------\n")
        response = 0
        while response < 1 or response > count:
            response = input("    Type a number (1-%s): " % count)
            return response
        return response

    def clean_as_float(self, value):
        try:
            return float(value)
        except Exception as exp:
            return None

    def run(self):
        try:
            while True:
                choices = [
                    "For Resistance",
                    "For Current",
                    "For Voltage",
                    "Press 4 or q for quit or Ctrl + C"
                ]
                option = self.menu("Enter the Option you want to Calculate \n ", choices=choices)
                if option == "1":
                    voltage = input("Enter Voltage: ")
                    voltage = self.clean_as_float(voltage)
                    if voltage is None:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    current = input("Enter Current: ")
                    current = self.clean_as_float(current)
                    if not current:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    digital = Digital()
                    digital.set_voltage(voltage)
                    digital.set_current(current)
                    digital.calculate_resistance()
                    resistance = digital.get_resistance()
                    print("\nResult:")
                    print("=================================")
                    print("\nVoltage: %s and Current:%s is Resistance: %s" % (digital.get_voltage(), digital.get_current(), digital.get_resistance()))
                    continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    while continue_option not in ["Y", "y", "N", "n"]:
                        print("The letter entered is not correct\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    if continue_option in ["Y", "y"]:
                        pass
                    elif continue_option in ["N", "n"]:
                        break
                elif option == "2":
                    voltage = input("Enter Voltage: ")
                    voltage = self.clean_as_float(voltage)
                    if voltage is None:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    resistance = input("Enter Resistance: ")
                    resistance = self.clean_as_float(resistance)
                    if not resistance:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    digital = Digital()
                    digital.set_voltage(voltage)
                    digital.set_resistance(resistance)
                    digital.calculate_current()
                    current = digital.get_current()
                    print("\nResult:")
                    print("=================================")
                    print("Voltage: %s and Resistance: %s is Current: %s" % (digital.get_voltage(), digital.get_resistance(), current))
                    continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    while continue_option not in ["Y", "y", "N", "n"]:
                        print("The letter entered is not correct\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    if continue_option in ["Y", "y"]:
                        pass
                    elif continue_option in ["N", "n"]:
                        break
                elif option == "3":
                    current = input("Enter Current: ")
                    current = self.clean_as_float(current)
                    if not current:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    resistance = input("Enter Resistance: ")
                    resistance = self.clean_as_float(resistance)
                    if not resistance:
                        print("\nError:")
                        print("=================================")
                        print("\nInvalid value entered. Please enter a valid value\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        while continue_option not in ["Y", "y", "N", "n"]:
                            print("The letter entered is not correct\n")
                            continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                        if continue_option in ["Y", "y"]:
                            pass
                        elif continue_option in ["N", "n"]:
                            break
                        continue
                    digital = Digital()
                    digital.set_current(current)
                    digital.set_resistance(resistance)
                    digital.calculate_voltage()
                    voltage = digital.get_voltage()
                    print("Current: %s and Resistance: %s is Voltage: %s" % (digital.get_current(), digital.get_resistance(), voltage))
                    continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    while continue_option not in ["Y", "y", "N", "n"]:
                        print("The letter entered is not correct\n")
                        continue_option = input("\nDo you wish to continue?(Y/N) or (y/n): ")
                    if continue_option in ["Y", "y"]:
                        pass
                    elif continue_option in ["N", "n"]:
                        break
                elif option in ["4", "q", "Q"]:
                    print("Quit requested. Bye...")
                    break
        except KeyboardInterrupt:
            print("Quit requested. Aborting now...")
            exit()
        except Exception as exp:
            print("Exception occured. Message: %s" % str(exp))
            exit()

if __name__ == "__main__":
    program = Program()
    program.run()