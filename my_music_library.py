from datetime import datetime


class Node(object):
    def __init__(self, tune_id=None, tune_name=None, tune_group=None, tune_genre=None,
                 date_entry=None, chart_position=None, price=None, next_node=None):
        self.__tune_id = tune_id
        self.__tune_name = tune_name
        self.__tune_group = tune_group
        self.__tune_genre = tune_genre
        self.__date_entry = date_entry
        self.__chart_position = chart_position
        self.__price = price
        self.next_node = next_node

    def clean_int(self, value):
        try:
            return int(value)
        except Exception as exp:
            return None

    def clean_float(self, value):
        try:
            return float(value)
        except Exception as exp:
            return None

    def clean_datetime(self, value):
        try:
            if hasattr(value, 'hour'):
                return value.strftime("%d-%m-%Y %H:%M:%S")
        except Exception as exp:
            return None

    def clean_string(self, value):
        try:
            return value
        except Exception as exp:
            return None

    def clean_fields(self, tune_id=None, tune_name=None, tune_group=None, tune_genre=None,
                 date_entry=None, chart_position=None, price=None):
        all_cleaned = False

        return all_cleaned

    def clean(self, attribute_name, value):
        methods = {
            "tune_id": self.clean_int,
            "tune_name": self.clean_string,
            "tune_group": self.clean_string,
            "tune_genre": self.clean_string,
            "date_entry": self.clean_datetime,
            "chart_position": self.clean_int,
            "price": self.clean_float,
        }
        if attribute_name not in methods:
            raise ValueError("Invalid attribute name. Valied names are: %s" % (str(methods.keys())))
        return value #methods[attribute_name](value)

    def get_next(self):
        return self.next_node

    def set_next(self, new_next):
        self.next_node = new_next

    """This method perform the match to this Node using attribute values provided as parameters."""
    def match(self, tune_id=None, tune_name=None, tune_group=None, tune_genre=None,
                 date_entry=None, chart_position=None, price=None):
        if tune_id:
            tune_id = self.clean(attribute_name="tune_id", value=tune_id)
            return self.__tune_id == tune_id if self.__tune_id else False
        elif tune_name:
            return tune_name in self.__tune_name if self.__tune_name else False
        elif tune_group:
            return tune_group in self.__tune_group if self.__tune_group else False
        elif tune_genre:
            return tune_genre in self.__tune_genre if self.__tune_genre else False
        elif date_entry:
            date_entry = self.clean(attribute_name="date_entry", value=date_entry)
            return self.__date_entry == date_entry
        elif chart_position:
            chart_position = self.clean(attribute_name="chart_position", value=chart_position)
            return self.__chart_position == chart_position
        elif price:
            price = self.clean(attribute_name="price", value=price)
            return self.__price == price
        return False

    def get(self, attribute_name):
        try:
            return getattr(self, attribute_name)
        except AttributeError as aerr:
            print("Attribute Error")
            raise AttributeError("No attribute: %s" % attribute_name)

    @property
    def tune_id(self):
        return self.__tune_id

    @property
    def tune_name(self):
        return self.__tune_name

    @property
    def tune_group(self):
        return self.__tune_group

    @property
    def tune_genre(self):
        return self.__tune_genre

    @property
    def date_entry(self):
        return self.__date_entry

    @property
    def chart_position(self):
        return self.__chart_position

    @property
    def price(self):
        return self.__price

    def __str__(self):
        display_string = """
            Tune Name: %s\n
            Tune Group: %s\n
            Tune Genre: %s\n
            Date Entry: %s\n
            Chart Position: %s\n
            Price: %s\n
            """ % (self.__tune_name, self.__tune_group, self.__tune_genre,
                   self.__date_entry, self.__chart_position, self.__price)
        return display_string

    def print_node(self):
        print(self)


class MyMusicLibrary(object):
    def __init__(self, head=None):
        self.head = head

    """Print the given items"""
    def print_items(self, items):
        print("\nTotal %s items" % len(items))
        for index, item in enumerate(items):
            print("\nItem: %s" % (index + 1))
            print(item)

    """Print the entire library."""
    def print_library(self):
        temp_pointer = self.head
        if not temp_pointer:
            print("\nNo items in the library")
            return
        index = 1
        while temp_pointer:
            print("\n==================================")
            print("\nItem: %s" % index)
            print(temp_pointer)
            print("\n==================================")
            temp_pointer = temp_pointer.next_node
            index += 1
        print("Total %s items" % (index - 1))

    # Linked List Insertion
    def insert_at_head(self, **data):
        # This method insert the data at the beginning of the list. First
        # creates a new node using the provided data and insert it at the beginning.
        new_node = Node(**data)
        if not self.head:
            self.head = new_node
        else:
            new_node.set_next(self.head)
            self.head = new_node

    def insert_at_end(self, **data):
        # This method insert the data at the end of the list. First
        # creates a new node using the provided data and insert it at the end of the library.
        new_node = Node(**data)
        if not self.head:
            self.insert_at_head(**data)
        else:
            temp = self.head
            while temp.next_node is not None:
                temp = temp.next_node
            temp.next_node = new_node
            new_node.set_next(None)

    def insert_at_middle(self, **data):
        # This method insert the data in the middle of the list.
        new_node = Node(**data)
        if not self.head:
            self.head = new_node
        else:
            slow_pointer, fast_pointer = self.head, self.head.next_node
            while fast_pointer and fast_pointer.next_node:
                slow_pointer = slow_pointer.next_node
                fast_pointer = fast_pointer.next_node.next_node
            new_node.next_node = slow_pointer.next_node
            slow_pointer.next_node = new_node

    # Linked List Searching
    def search_item(self, attribute_name, search_key, return_node=True, match_many=False):
        # This method do a linear search starting from the head till the end or until it finds the element.
        current = self.head
        found = False
        matched_node = None
        match_items = []
        data = {"%s" % attribute_name: "%s" % search_key}
        if not current:
            # print("Library is empty")
            return False
        while current:
            if current.match(**data):
                found = True
                matched_node = current
                if match_many:
                    match_items += [matched_node]
                else:
                    break
            current = current.next_node
        if not found:
            if match_many:
                return []
            else:
                return None
        return_value = matched_node if return_node else current
        if match_many:
            return_value = match_items
        return return_value

    """
    tune_id=None, tune_name=None, tune_group=None, tune_genre=None,
                 date_entry=None, chart_position=None, price=None

    submethod which calls the generic method search item specifically for a search key
    """
    def search_by_tune_id(self, tune_id):
        return self.search_item(attribute_name="tune_id", search_key=tune_id)

    def search_by_tune_name(self, tune_name):
        return self.search_item(attribute_name="tune_name", search_key=tune_name)

    def search_by_tune_group(self, tune_group):
        return self.search_item(attribute_name="tune_group", search_key=tune_group)

    def search_by_tune_genre(self, tune_genre):
        return self.search_item(attribute_name="tune_genre", search_key=tune_genre, match_many=True)

    def search_by_date_entry(self, date_entry):
        return self.search_item(attribute_name="date_entry", search_key=date_entry)

    def search_by_chart_position(self, chart_position):
        return self.search_item(attribute_name="chart_position", search_key=chart_position)

    def search_by_price(self, price):
        return self.search_item(attribute_name="price", search_key=price)

    # Delete Item

    def delete_first(self):
        # Delete the first item
        if not self.head:
            return False
        self.head = self.head.next_node
        return True

    def delete_last(self):
        # Delete the last item
        if not self.head:
            return False
        else:
            temp = self.head
            prev = temp
            while temp.next_node is not None:
                prev = temp
                temp = temp.next_node
            prev.next_node = temp.next_node
            return True

    def find_and_delete(self, attribute_name, search_key):
        # First find the element using the attribute name and search key and then delete the item
        current = self.head
        previous = None
        found = False
        data = {"%s" % attribute_name: "%s" % search_key}
        if not current:
            print("\nLibrary is empty")
            return False
        while current:
            if current.match(**data):
                print("\nFound the item. Deleting it now...")
                found = True
                break
            previous = current
            current = current.next_node
        if found:
            if not previous:
                self.head = current.next_node
                return True
            else:
                previous.next_node = current.next_node
                return True
        return False

    # The following are the submethods for find_and_delete which specifies the attribute name and search key
    def delete_by_tune_id(self, tune_id):
        return self.find_and_delete(attribute_name="tune_id", search_key=tune_id)

    def delete_by_tune_name(self, tune_name):
        return self.find_and_delete(attribute_name="tune_name", search_key=tune_name)

    def delete_by_tune_group(self, tune_group):
        return self.find_and_delete(attribute_name="tune_group", search_key=tune_group)

    def delete_by_tune_genre(self, tune_genre):
        return self.find_and_delete(attribute_name="tune_genre", search_key=tune_genre)

    def delete_by_date_entry(self, date_entry):
        return self.find_and_delete(attribute_name="date_entry", search_key=date_entry)

    def delete_by_chart_position(self, chart_position):
        return self.find_and_delete(attribute_name="chart_position", search_key=chart_position)

    def delete_by_price(self, price):
        return self.find_and_delete(attribute_name="price", search_key=price)

    def sort_items(self, attribute_name):
        # This method sort items using insertion sort algorithm.
        if self.head is None:
            print("Library is empty")
            return
        if self.head.next_node is None:
            # Only one item in the library
            return
        else:
            prev = self.head
            current = self.head.next_node
            while current is not None:
                if prev.get(attribute_name) > current.get(attribute_name):
                    print("head: " + str(self.head.get(attribute_name)))
                    print("prev: " + str(prev.get(attribute_name)))
                    print("current: " + str(current.get(attribute_name)))

                    print("  ")

                    prev.next_node = current.next_node

                    current_1 = self.head
                    next = self.head.next_node
                    while current_1 != prev.next_node:
                        if current.get(attribute_name) < self.head.get(attribute_name):
                            current.next_node = self.head
                            self.head = current
                            break
                        elif current.get(attribute_name) < next.get(attribute_name):
                            current.next_node = next
                            current_1.next_node = current
                            break
                        current_1 = next
                        next = current_1.next_node
                prev = current
                current = current.next_node

    def sort_by_song(self):
        self.sort_items(attribute_name="tune_name")

    def sort_by_genre(self):
        self.sort_items(attribute_name="tune_genre")

    def sort_by_group(self):
        self.sort_items(attribute_name="tune_group")

    def is_empty(self):
        return self.head is None


class Program(object):
    def __init__(self):
        self.my_music_library = MyMusicLibrary()

    def get_main_menu_options(self):
        options = [
            "Add a New Music",
            "Display All Music",
            "Search Music by Genre(e.g pop, rock etc)",
            "Search music by Name",
            "Delete Music Item",
            "Sort Music Item(asc)",
            "Quit(Ctrl + C) or press 'q'"
        ]
        return options

    def get_add_music_submenu(self):
        return [
            "Add in the beginning",
            "Add in the end",
            "Add in the middle",
            "Return to the main menu"
        ]

    def menu(self, prompt, choices):
        print("\n%s\n" % prompt)
        count = len(choices)
        for i in range(count):
            print("(%s) %s" % (i + 1, choices[i]))
        response = 0
        while response < 1 or response > count:
            response = input("    Type a number (1-%s): " % count)
            return response
        return response

    def handle_single_input(self, prompt):
        value = input(prompt)
        return value
        # if tune_id.isdigit():
        #     tune_id = validator(tune_id)
        # else:
        #     print("\nTune id must be integer. Please enter a valid tune id")

    def take_item_input(self):
        tune_id = self.handle_single_input("  Enter tune id: ")
        tune_name = self.handle_single_input("  Enter tune name: ")
        tune_group = self.handle_single_input("  Enter tune group: ")
        tune_genre = self.handle_single_input("  Enter tune genre: ")
        date_entry = self.handle_single_input("  Enter date entry: ")
        chart_position = self.handle_single_input("  Enter chart position: ")
        price = self.handle_single_input("  Enter price: ")
        return {
                "tune_id" : tune_id,
                "tune_name" : tune_name,
                "tune_group" : tune_group,
                "tune_genre": tune_genre,
                "date_entry":  date_entry,
                "chart_position": chart_position,
                "price" : price
            }

    def handle_insert_option(self):
        valid_option = False
        while not valid_option:
            submenu_response = self.menu("How do you want to insert the music in the library?",
                                         self.get_add_music_submenu())
            if submenu_response == "1":
                data = self.take_item_input()
                self.my_music_library.insert_at_head(**data)
                print("Item inserted at the beginning")
                self.my_music_library.print_library()
                valid_option = True
            elif submenu_response == "2":
                data = self.take_item_input()
                self.my_music_library.insert_at_end(**data)
                print("Item inserted at the end")
                self.my_music_library.print_library()
                valid_option = True
            elif submenu_response == "3":
                data = self.take_item_input()
                self.my_music_library.insert_at_middle(**data)
                print("Item inserted in the middle")
                self.my_music_library.print_library()
                valid_option = True
            elif submenu_response == "4":
                print("\nReturning to the main menu")
                valid_option = True
            else:
                print("\nInvalid option. Please enter the correct option")

    def handle_search_by_genre_option(self):
        self.menu("Please enter genre name: ", self.get_main_menu_options())

    def run(self):
        try:
            while True:
                main_menu_response = self.menu("Please enter your choice: ", self.get_main_menu_options())
                if main_menu_response == "1":
                    self.handle_insert_option()
                elif main_menu_response == "2":
                    self.my_music_library.print_library()
                elif main_menu_response == "3":
                    genre_name = input("\n  Please enter genre name: ")
                    if self.my_music_library.is_empty():
                        print("\nOutput: ")
                        print("\nLibrary is empty")
                    else:
                        print("\nOutput: ")
                        items = self.my_music_library.search_by_tune_genre(tune_genre=genre_name)
                        if items:
                            self.my_music_library.print_items(items)
                        else:
                            print("\nNo items found with genre: %s" % genre_name)
                elif main_menu_response == "4":
                    tune_name = input("\n  Please enter music name: ")
                    if self.my_music_library.is_empty():
                        print("\nOutput: ")
                        print("\nLibrary is empty")
                    else:
                        item = self.my_music_library.search_by_tune_name(tune_name=tune_name)
                        print("\nOutput: ")
                        if item:
                            print(item)
                        else:
                            print("\nNo item found with name: %s" % tune_name)
                elif main_menu_response == "5":
                    delete_option = self.menu("How do you want to delete?: ", ["From Start", "From End", "Search"])
                    if delete_option == "1":
                        if self.my_music_library.is_empty():
                            print("\nOutput: ")
                            print("\nLibrary is empty")
                        else:
                            print("\nOutput: ")
                            self.my_music_library.delete_first()
                            self.my_music_library.print_library()
                    elif delete_option == "2":
                        if self.my_music_library.is_empty():
                            print("\nOutput: ")
                            print("\nLibrary is empty")
                        else:
                            print("\nOutput: ")
                            self.my_music_library.delete_last()
                            self.my_music_library.print_library()
                    elif delete_option == "3":
                        print("\n Note: The first item that match the search will be deleted from the library.")
                        delete_search_option = self.menu("\n How do you want to search the item by? ", ["By Name", "By Genre", "By Group"])
                        if delete_search_option == "1":
                            search_term =  input("\n  Please enter music name: ")
                            if self.my_music_library.is_empty():
                                print("\nOutput: ")
                                print("\nLibrary is empty")
                            else:
                                print("\nItem deleted")
                                print("\nOutput: ")
                                self.my_music_library.delete_by_tune_name(tune_name=search_term)
                                self.my_music_library.print_library()
                        elif delete_search_option == "2":
                            search_term = input("\n  Please enter tune genre: ")
                            if self.my_music_library.is_empty():
                                print("\nOutput: ")
                                print("\nLibrary is empty")
                            else:
                                print("\nItem deleted")
                                print("\nOutput: ")
                                self.my_music_library.delete_by_tune_genre(tune_genre=search_term)
                                self.my_music_library.print_library()
                        elif delete_search_option == "3":
                            search_term = input("\n  Please enter tune group: ")
                            if self.my_music_library.is_empty():
                                print("\nOutput: ")
                                print("\nLibrary is empty")
                            else:
                                print("\nItem deleted")
                                print("\nOutput: ")
                                self.my_music_library.delete_by_tune_group(tune_group=search_term)
                                self.my_music_library.print_library()
                        else:
                            print("\n Invalid option")
                    else:
                        print("\nInvaid option")
                elif main_menu_response == "6":
                    sort_option = self.menu("\n How do you want to sort the item by? ", ["By Name", "By Genre", "By Group"])
                    if sort_option == "1":
                        self.my_music_library.sort_by_song()
                        if self.my_music_library.is_empty():
                            print("\nLibrary is empty")
                        else:
                            print("\nOutput: ")
                            self.my_music_library.print_library()
                    elif sort_option == "2":
                        self.my_music_library.sort_by_genre()
                        if self.my_music_library.is_empty():
                            print("\nLibrary is empty")
                        else:
                            print("\nOutput: ")
                            self.my_music_library.print_library()
                    elif sort_option == "3":
                        self.my_music_library.sort_by_group()
                        if self.my_music_library.is_empty():
                            print("\nLibrary is empty")
                        else:
                            print("\nOutput: ")
                            self.my_music_library.print_library()
                    else:
                        print("\nInvalid option")
                elif main_menu_response in ["7", "Q", "q"]:
                    print("Quit requested. Aborting now...")
                    exit()
                # self.handle_main_menu_option(option)
        except KeyboardInterrupt as kerr:
            print("Quit requested. Aborting now...")
            exit()
        except Exception as exp:
            print("Exception occured. Message: %s" % str(exp))


if __name__ == "__main__":
    program = Program()
    program.run()